from random import random

import numpy as np
from cec17_functions import cec17_test_func


class Individual:
    def __init__(self, dimensions=None, minimum=None, maximum=None, fit_function=None, values=None):
        self.dimensions = dimensions
        self.maximum = maximum
        self.minimum = minimum
        if values is not None:
            self.values = values
        else:
            self.values = []
            self.values.append(self.generate_values(dimensions, minimum, maximum))
            self.values.append(self.generate_deviations(dimensions))
        self.fit = self.evaluate(fit_function)

    def generate_values(self, dimensions, minimum, maximum):
        # do generowania wartości wykorzystujemy rozkład jednostajny, daje on wiekszą szasne wypełnienia całej
        # przestrzeni
        values = np.random.uniform(minimum, maximum, size=dimensions).tolist()
        return values

    def generate_deviations(self, dimensions):
        # do generowania odchyleń wykorzystujemy rozkład jednostajny
        deviations = np.random.uniform(0, 1, size=dimensions).tolist()
        return deviations

    def evaluate(self, fit_function):
        # mx: Number of objective functions
        mx = 20
        # Pointer for the calculated fitness
        f = [0]
        cec17_test_func(self.values[0], f, self.dimensions, mx, fit_function)  # 13 i 18 i 5 i 6
        return f[0]

    def check_space_constraints(self):
        for i in range(0, self.dimensions):
            if self.values[0][i] > self.maximum:
                self.values[0][i] = self.maximum
            if self.values[0][i] < self.minimum:
                self.values[0][i] = self.minimum
