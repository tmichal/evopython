from SingleEvolution import SingleEvolution

"""Klasa reprezentujaca model wyspowy algorytmu ewolucyjnego."""
class IslandEvolution:
    def __init__(self, population_size, dimensions, minimum, maximum, fit_function, maximizing, island_count, mate_param=0.5, migration_param=5):
        """Lista zawierajaca listy analizowanych populacji(wysp) i ich parametrow.
        
        Zawartosc przechowywanych list:
        - Obiekty pojedynczych przypadkow ewolucji populacji (wysp), 
        - Wartosci bool dla kazdego przypadku stwierdzajace, czy nalezy wykonac kolejny krok ewolucji,
        - Dodatkowe wartosci""" #todo
        self.single_evolutions = []
        """"""
        self.size = population_size
        """"""
        self.dimensions = dimensions
        """"""
        self.minimum = minimum
        """"""
        self.maximum = maximum
        """"""
        self.fit_function = fit_function
        """"""
        self.maximizing = maximizing
        """"""
        self.island_count = island_count
        """Atrybut okreslajacy maksymalna liczbe krokow w procesie ewolucji.
        
        Wartosc wyliczana jest na podstawie rozmiaru populacji oraz liczby wymiarow danych."""
        self.max_step_count = round(10000 * dimensions / self.size)
        """"""
        self.mate_param = mate_param
        """
        
        Wartosc czestosci migracji nie moze byc nizsza niz 5 krokow. W przypadku pojawienia sie wartosci mniejszej 
        w wywolaniu, zmieniana jest ona na wartosc minimalna (5)."""
        if migration_param < 5:
            self.migration_param = 5 #czestotliwosc migracji
        else:
            self.migration_param = migration_param
        """"""
        self.results = [] #todo: chcemy to zostawic?

    """Glowna metoda odpowiedzialna za uzycie algorytmu ewolucyjnego dla modelu wyspowego.

    Metoda wywolywana jest na utworzonym wczesniej obiekcie modelu wyspowego, zawierajacym wszystkie konieczne parametry
    do poprawnego dzialania algorytmu. Najpierw tworzone sa w niej wyspy podlegajace badaniom. Następnie zawarta zostala
    petla odpowiadajaca za wykonanie kolejnego kroku dla kazdej populacji. Sprawdzany jest przy tym warunek na
    wystapienie migracji osobnikow oraz wystapienie stanu terminalnego."""
    def perform_evolution(self):
        single = [SingleEvolution(self.size, self.dimensions, self.minimum, self.maximum, self.fit_function, self.maximizing, str(i), self.mate_param) for i in range(self.island_count)]
        self.single_evolutions.append(single) #todo w sumie to wszystko powinno byc gdzie indziej
        self.single_evolutions.append([True] * self.island_count) # warunek wykonywania kolejnego kroku
        self.single_evolutions.append([0] * self.island_count) # ile razy powtorzyl sie najlepszy wynik #todo co z tym robimy
        g=0

        while g < self.max_step_count:
            if self.check_stop_constraints() == True: #jezeli jakakolwiek wyspa zostanie ukonczona to zakoncz poszukiwanie
                for j in range(self.island_count): # wykonywanie kroku ewolucji dla kazdej populacji
                    if self.single_evolutions[1][j] == True: #todo w sumie ten warunek juz nie jest CHYBA potrzebny
                        new_best_fit = self.single_evolutions[0][j].single_evolution_step() # wykonanie kroku ewolucji
                        # sprawdzenie czy uzyskany zostal stan terminalny
                        self.single_evolutions[1][j] = self.single_evolutions[0][j].check_stop_conditions_2(self.single_evolutions[1][j], new_best_fit)
                        self.single_evolutions[0][j].step_count = g+1 # inkrementacja licznika wykonanych krokow ewolucji

                if ((g+1) % self.migration_param) == 0 and g != 0: #warunek sprawdzajacy czy w danym kroku powinna wystapic mutacja
                    self.migrate_best_individual()

                g=g+1
            else:
                g=self.max_step_count # zatrzymanie petli poszukiwan ze wzgledu na odnalezienie rozwiazania

        if self.check_stop_constraints() == False: #todo co z tym
            for i in range(self.island_count):
                if self.single_evolutions[1][i] == False:
                    self.results.append(self.single_evolutions[0][i].step_count)
                    self.results.append(self.single_evolutions[0][i].best_fit - self.single_evolutions[0][i].expected_fit_function_minimum)
                    self.results.append(self.dimensions)
                    self.results.append(self.fit_function)
                    self.results.append(self.island_count)
                    self.results.append(self.size)
                    self.results.append(self.mate_param)
                    self.results.append(self.migration_param)
        else:
            best = self.find_best_not_ended()
            self.results.append(self.single_evolutions[0][0].step_count)
            self.results.append(best - self.single_evolutions[0][0].expected_fit_function_minimum)
            self.results.append(self.dimensions)
            self.results.append(self.fit_function)
            self.results.append(self.island_count)
            self.results.append(self.size)
            self.results.append(self.mate_param)
            self.results.append(self.migration_param)
            # print("Populacja: ", self.single_evolutions[0][i].name)
            # print("Czy nadal poszukiwane: ", self.single_evolutions[1][i])
            # print("Ile krokow: ", self.single_evolutions[0][i].step_count)
            # print("Wynik: ", self.single_evolutions[0][i].best_fit)
            # for j in range(self.size):
            #     print(self.single_evolutions[0][i].population.individuals[j].fit)

        self.results.append(self.mate_param)
        return self.results

    """Metoda umozliwiajaca migracje najlepszego osobnika do sasiedzkiej(n+1) populacji."""
    def migrate_best_individual(self):
        for i in range(self.island_count):
            self.single_evolutions[0][i].population.sort_individuals()

        for i in range(self.island_count): #todo zapomnialam zrobic migracje dla maksymalizacji
            if i==(self.island_count -1):
                self.single_evolutions[0][0].take_best_individual(self.single_evolutions[0][i].get_best_individual())
            else:
                self.single_evolutions[0][i+1].take_best_individual(self.single_evolutions[0][i].get_best_individual())

        for i in range(self.island_count):
            self.single_evolutions[0][i].population.sort_individuals()

    """Metoda umozliwiajaca sprawdzenie wystapienia warunku terminalnego (odnalezienie minimum) dla modelu wyspowego."""
    def check_stop_constraints(self):
        condition = True
        for i in range(self.island_count):
            if self.single_evolutions[1][i] == False:
                condition = False
        return condition

    """Metoda umozliwiajaca wybor populacji o najlepszej wartosci funkcji przystosowania w przypadku gdy nie wystapil 
    stan terminalny odnalezienia rzeczywistego minimum"""
    def find_best_not_ended(self):
        if self.maximizing:
            best = -100000000
            for i in  range(self.island_count):
                if self.single_evolutions[0][i].best_fit > best:
                    best = self.single_evolutions[0][i].best_fit
        else:
            best = 100000000
            for i in  range(self.island_count):
                if self.single_evolutions[0][i].best_fit < best:
                    best = self.single_evolutions[0][i].best_fit
        return best
